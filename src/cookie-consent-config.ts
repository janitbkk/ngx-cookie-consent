import { InjectionToken } from '@angular/core';

export const COOKIE_CONSENT_CONFIG = new InjectionToken('CookieConsentConfigToken');

export interface CookieConsentConfig {
  version?: string;
  baseUrl: string;
  name?: string;
  value?: any;
  path?: string;
  expires?: number | Date;
  domain?: string;
  secure?: boolean;
  learnMore: {
    internalLink?: string;
    externalLink?: string;
  };
}
