import { Component, ChangeDetectionStrategy } from '@angular/core';
import { trigger, transition, style, animate, state } from '@angular/animations';
import { Observable } from 'rxjs';

import { CookieConsentService } from '../../cookie-consent.service';
import { CookieConsentConfig } from '../../cookie-consent-config';

@Component({
  selector: 'casdl-cc-mono',
  templateUrl: './cc-mono.component.html',
  styleUrls: ['./cc-mono.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('slide', [
      transition(':enter', [
        style({transform: 'translateY(100%)', opacity: 0}),
        animate('500ms', style({transform: 'translateY(0)', 'opacity': 1}))
      ]),
      transition(':leave', [
        style({transform: 'translateY(0)', 'opacity': 1}),
        animate('500ms', style({transform: 'translateY(100%)', 'opacity': 0}))
      ])
    ])
  ]
})
export class CcMonoComponent {
  public showNotification: Observable<boolean> = this.cookieConsentService.showNotification;
  public config: CookieConsentConfig = this.cookieConsentService.config;

  get learnMoreIsInternal() {
    return this.config.learnMore.internalLink ? true : false;
  }

  get text() {
    return this.cookieConsentService.policyIsUpdated()
      ? 'Our cookies policy have been updated and by using the site you agree to this.'
      : 'Our website uses cookies and by using the site you agree to this.';
  }

  constructor(private cookieConsentService: CookieConsentService) {}

  dismiss() {
    this.cookieConsentService.dismiss();
  }

}
