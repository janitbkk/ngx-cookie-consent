import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { CookieService } from './cookie.service';
import { CcMonoComponent } from './components/cc-mono/cc-mono.component';
import { CookieConsentService } from './cookie-consent.service';
import { CookieConsentConfig, COOKIE_CONSENT_CONFIG } from './cookie-consent-config';

export * from './cookie-consent-config';
export * from './components/cc-mono/cc-mono.component';
export * from './cookie-consent.service'
export * from './cookie.service';

@NgModule({
  imports: [CommonModule, RouterModule],
  exports: [CcMonoComponent],
  declarations: [CcMonoComponent]
})
export class CookieConsentModule {

  static forRoot(cookieConsentConfig: CookieConsentConfig): ModuleWithProviders {
    return {
      ngModule: CookieConsentModule,
      providers: [
        { provide: COOKIE_CONSENT_CONFIG, useValue: cookieConsentConfig },
        CookieConsentService, CookieService
      ]
    };
  }

  constructor(@Optional() @SkipSelf() parentModule: CookieConsentModule) {
    if (parentModule) {
      throw new Error('CookieConsentModule is already loaded. Import it only once!');
    }
  }
}
