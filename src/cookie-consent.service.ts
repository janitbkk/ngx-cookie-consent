import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Observable, BehaviorSubject } from 'rxjs';

import { CookieService } from './cookie.service';

import { COOKIE_CONSENT_CONFIG, CookieConsentConfig } from './cookie-consent-config';

@Injectable()
export class CookieConsentService {
  private _showNotification: BehaviorSubject<boolean> = new BehaviorSubject(true);
  public readonly showNotification: Observable<boolean> = this._showNotification.asObservable();

  get isBrowser() {
    return isPlatformBrowser(this.platformId);
  }

  get baseUrlExists() {
    return this.config && this.config.baseUrl ? true : false;
  }

  config: CookieConsentConfig;
  defaultConfig: CookieConsentConfig = {
    baseUrl: '',
    version: '1',
    name: 'cl-cc',
    path: '/',
    expires: 1000,
    domain: this.baseUrlExists ? new URL(this.config.baseUrl).origin : '',
    secure: this.baseUrlExists && new URL(this.config.baseUrl).protocol === 'https' ? true : false,
    learnMore: {}
  };

  constructor(
    private cookieService: CookieService,
    @Inject(COOKIE_CONSENT_CONFIG) config: CookieConsentConfig,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    this.config = { ...this.defaultConfig, ...config };

    if (this.cookieExists() && !this.policyIsUpdated()) {
      this.hideBanner();
    } else {
      this.revealBanner();
    }
  }

  dismiss() {
    if (!this.isBrowser) { return; }

    this.cookieService.set(
      this.config.name,
      this.config.version,
      this.config.expires,
      this.config.path,
      this.config.domain,
      this.config.secure
    );

    this.hideBanner();
  }

  revealBanner() {
    this._showNotification.next(true);
  }

  hideBanner() {
    this._showNotification.next(false);
  }

  removeCookie() {
    if (!this.isBrowser) { return; }
    this.cookieService.delete(this.config.name);
  }

  cookieExists(): boolean {
    if (!this.isBrowser) { return; }
    return this.cookieService.check(this.config.name);
  }

  policyIsUpdated(): boolean {
    if (!this.isBrowser) { return; }
    return this.cookieExists() &&
      this.cookieService.get(this.config.name) != this.config.version;
  }
}
